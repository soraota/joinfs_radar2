using UnityEngine;
using System;

public class ConvertPosition : MonoBehaviour
{
    //日本の緯度経度を定義
    const double referenceLatitude = 35.6894875 * (Math.PI / 180);      //ラジアン化
    const double referenceLongitude = 139.6917064;

    //public float earth_radius = 6378137;

    private void Start()
    {

    }
    private void Update()
    {

    }
    public Vector2 GetPos(double latitude, double longitude)
    {
        //投影
        float a = (float)latitude * ((float)Math.PI / 180);
        float m1 = a / 2.0f;
        float m2 = Mathf.Tan(m1 + (Mathf.PI / 4.0f));
        double x = longitude - referenceLongitude;
        double y = Mathf.Log(m2) - Mathf.Log(Mathf.Tan((float)referenceLatitude / 2.0f) + (Mathf.PI/4.0f));

        return new Vector2((float)x, (float)y);
    }
}