using Assets.src;
using UnityEngine;
using Debug = UnityEngine.Debug;

public class Airplane : MonoBehaviour
{
    private float repeatSpan;
    private float timeElapsed;
    private int num_Airplanes;
    public Brigde.Airplane_Structure airplaneStructure;
    Heading heading_script;
    GameObject Heading_obj;
    Vector3 Direction;
    // Start is called before the first frame update
    void Start()
    {
        //Headingオブジェクトとスクリプトを取得
        Heading_obj = transform.GetChild(0).gameObject;
        heading_script = Heading_obj.GetComponent<Heading>();

        repeatSpan = 2.5f;
        timeElapsed = 2;
    }

    // Update is called once per frame
    void Update()
    {
        timeElapsed += Time.deltaTime;
        if (timeElapsed >= repeatSpan)
        {
            //Debug_fn();
            update_heading();
            timeElapsed = 0;
        }
    }
    private void Debug_fn()
    {
        Debug.Log(airplaneStructure.lat);
    }
    private void update_heading()
    {
        //Whazzupから取得した方位と高度を渡す
        heading_script.angle = airplaneStructure.heading;
        heading_script.alt = airplaneStructure.alt;
        heading_script.Airplane_direction = this.transform.right;
    }
}
