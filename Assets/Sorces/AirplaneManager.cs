using Assets.src;
using System.Collections.Generic;
using UnityEngine;
public class AirplaneManager : MonoBehaviour
{
    GameObject Brigde;
    GameObject ConvertPosition;
    GameObject prefab_airplane;
    GameObject MVA;
    Brigde brigde_tool;
    ConvertPosition ConvPos_tool;

    private float repeatSpan;
    private float timeElapsed;
    private int num_Airplanes;

    List<string> airplane_objList = new List<string>();
    List<string> removeList = new List<string>();

    // Start is called before the first frame update
    void Start()
    {
        //それぞれ必要なオブジェクトを検索及びロード
        Brigde = GameObject.Find("Brigde");
        ConvertPosition = GameObject.Find("ConvPos");
        brigde_tool = Brigde.GetComponent<Brigde>();
        ConvPos_tool = ConvertPosition.GetComponent<ConvertPosition>();
        prefab_airplane = (GameObject)Resources.Load("Airplane");
        MVA = GameObject.Find("MVA");

        //update内での待機時間を定義
        repeatSpan = 2.5f;
        timeElapsed = 2;
    }

    // Update is called once per frame
    void Update()
    {
        timeElapsed += Time.deltaTime;
        if (timeElapsed >= repeatSpan)
        {
            ManageAirplaneObject();
            timeElapsed = 0;
        }
    }
    void ManageAirplaneObject()
    {
        //Brigdeから関数を実行し、Aiprane_Structureを取得
        var lines = brigde_tool.Open_Whazzup();
        List<string> client_data_list = brigde_tool.Get_clients_data(lines);

        List<Brigde.Airplane_Structure> airplanes = brigde_tool.GetAirplanes();

        foreach (Brigde.Airplane_Structure airplane in airplanes)
        {
            //緯度経度をメルカトル図法を使いUnity座標に投影
            Vector3 Airplane_pos = ConvPos_tool.GetPos(airplane.lat, airplane.lon);
            
            //投影した座標を100倍にする
            Airplane_pos.x *= 100;
            Airplane_pos.y *= 100;

            /*Debug.Log(Airplane_pos);
            Debug.Log(brigde_tool.Get_num_PILOT(lines, client_data_list));
            Debug.Log(num_Airplanes);*/

            //接続されているパイロットの数が生成した飛行機オブジェクトの数よりも少なかったらオブジェクトを生成する
            if (brigde_tool.Get_num_PILOT(lines, client_data_list) > num_Airplanes)
            {
                //オブジェクトを生成して生成したオブジェクトの数(num_Airplanes)をインクリメントする
                num_Airplanes++;
                GameObject airplanes_obj = Instantiate(prefab_airplane, Airplane_pos, Quaternion.identity) as GameObject;
                airplanes_obj.name = airplane.callsign; //オブジェクトの名前をコールサインにして管理しやすくする

                //それぞれの飛行機オブジェクトにAirplane_Structure構造体を引き渡す
                Airplane airplane_script = airplanes_obj.GetComponent<Airplane>();
                airplane_script.airplaneStructure = airplane;

                airplane_objList.Add(airplanes_obj.name);
            }

            foreach (string callsign in airplane_objList)
            {
                //位置更新及び構造体のデータを更新
                if (airplane.callsign == callsign)
                {
                    var airplanes_obj = GameObject.Find(callsign);
                    if (airplanes_obj != null)
                    {
                        var airplane_obj_transform = airplanes_obj.transform;
                        airplane_obj_transform.position = Airplane_pos;
                        Airplane airplane_script = airplanes_obj.GetComponent<Airplane>();
                        airplane_script.airplaneStructure = airplane;
                    }
                    else
                    {
                        Debug.LogWarning("GameObject " + callsign + " not found.");
                    }
                }
                //接続が失われたオブジェクトを削除する
                else
                {
                    var airplanes_obj = GameObject.Find(callsign);
                    if (airplanes_obj != null)
                    {
                        removeList.Add(callsign);
                        DestroyObject(removeList);
                    }
                    else
                    {
                        Debug.LogWarning("GameObject " + callsign + " not found.");
                    }
                }
            }
        }
    }
    void DestroyObject(List<string> removeList)
    {
        //removeListに入っているオブジェクトを削除する
        foreach(string callsign in removeList)
        {
            var airplane_obj = GameObject.Find(callsign);
            Destroy(airplane_obj);
            num_Airplanes--;
        }
    }
}