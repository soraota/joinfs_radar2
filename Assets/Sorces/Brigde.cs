using UnityEngine;
using System;
using System.IO;
using System.Collections.Generic;

namespace Assets.src
{
	public class Brigde : MonoBehaviour
	{
		//Aiprane_Structure構造体を定義
		public struct Airplane_Structure
		{
			public string callsign { get; set; }
			public string mode { get; set; }
			public double lat { get; set; }
			public double lon { get; set; }
			public int alt { get; set; }
			public int gs { get; set; }
			public string type { get; set; }
			public int squawk { get; set; }
			public string flight_plan { get; set; }
			public string IorV { get; set; }
			public string origin { get; set; }
			public string destination { get; set; }
			public int heading { get; set; }
			public string remarks { get; set; }
		}

		// Use this for initialization
		void Start()
		{
			Debug.Log("Hello World!");
		}

        public String[] Open_Whazzup()
        {
			//Whazzup.txtを開き内容をlinesに格納
            string whazzupPATH = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\JoinFS\Test.txt";
            var lines = File.ReadAllLines(whazzupPATH);

            return lines;
        }

        public List<Airplane_Structure> GetAirplanes()
		{
			var Airplanes = new List<Airplane_Structure>();
			var lines = Open_Whazzup();

			List<string> client_data_list = Get_clients_data(lines);	//!CLIENTSの中身を一行ずつ配列として格納

			foreach (var client_data in client_data_list)
			{
				//!CLIENTSの中身を一行ごとに整形
				var split_data = client_data.Split(':');
				if (split_data[3] == "PILOT")
				{
					//Debug.Log("現在の接続されているパイロットの数:" + num_clients);

					var callsign = split_data[0];               // コールサイン
					var mode = split_data[3];                   // ATC or PILOT
					var lat = double.Parse(split_data[5]);      // 緯度
					var lon = double.Parse(split_data[6]);      // 経度
					var alt = int.Parse(split_data[7]);         // 高度
					var gs = int.Parse(split_data[8]);          // 対地速度
					var type = split_data[9];                   // 機体型
					var origin = split_data[11];                // 出発空港
					var destination = split_data[13];           // 到着空港
					var squawk = int.Parse(split_data[17]);     // スコークコード
					var IorV = split_data[21];                  // IFRorVFR
					var remarks = split_data[30];               // 特記事項
					var heading = int.Parse(split_data[38]);    // 方位

					string flight_plan;							//IFRの場合はフライトプランを格納しVFRの場合はVFRを格納する
					if (IorV == "VFR")
					{
						//Debug.Log("VFRで飛んでいます");
						flight_plan = "VFR";
					}
					else if (IorV == "IFR")
					{
						//Debug.Log("IFRで飛んでいます");
						flight_plan = split_data[30];
						//Debug.Log(flight_plan);
					}
					else
					{
						Debug.Log("エラーが発生しました");
						flight_plan = "";
					}
					Airplane_Structure airplane = new Airplane_Structure	//整形したデータを一機ずつ構造体で管理
					{
						callsign = callsign,
						mode = mode,
						lat = lat,
						lon = lon,
						alt = alt,
						gs = gs,
						type = type,
						origin = origin,
						destination = destination,
						flight_plan = flight_plan,
						remarks = remarks,
						squawk = squawk,
						IorV = IorV,
						heading = heading,
					};
					Airplanes.Add(airplane);
					//Debug.Log(airplane.alt);
				}
			}
			return Airplanes;
		}

		public int Get_num_clients(String[] lines)
        {
			//接続されているクライアントの数を取得
			int num_clients = 0;
			for (int i = 0; i < lines.Length; i++)
			{
				if (lines[i].Contains("CONNECTED CLIENTS"))
				{
					var split_line = lines[i].Split(' ');
					num_clients = int.Parse(split_line[3]);
					break;
				}
			}
			return num_clients;
		}

		public List<string> Get_clients_data(String[] lines)
        {
			//Whazzupの中の!CLIENTSの中身だけ取り出す
			var client_data_list = new List<string>();
			bool started = false;

			for (int i = 0; i < lines.Length; i++)
			{
				if (lines[i].StartsWith("!CLIENTS"))
				{
					started = true;
				}
				else if (lines[i].StartsWith("!SERVERS"))
				{
					break;
				}
				else if (started)
				{
					client_data_list.Add(lines[i]);
				}
			}
			return client_data_list;
		}

		public int Get_num_PILOT(String[] lines, List<string> client_data_list)
		{
			//接続されているパイロットの数を取得
			int num_PILOT = Get_num_clients(lines);
			foreach (var client_data in client_data_list)
			{
				var split_data = client_data.Split(':');
				if (split_data[3] == "ATC")
				{
					Debug.Log("接続されているクライアントはATCです");
					num_PILOT--;
					continue;
				}
			}
			return num_PILOT;
		}
	}
}