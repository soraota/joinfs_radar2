using System.Collections.Generic;
using UnityEngine;

public class MVA : MonoBehaviour
{
    public ConvertPosition convertPosition;
    public GameObject circlePrefab;
    public List<float> radii = new List<float>();
    public List<Vector3> centerPoints = new List<Vector3>();
    public int numSegments = 36;
    private bool updateCircles = true;

    private List<GameObject> circles = new List<GameObject>();

    void Start()
    {
        UpdateCircles();
    }

    void Update()
    {
        if (updateCircles)
        {
            UpdateCircles();
        }
    }

    void UpdateCircles()
    {
        if (radii.Count != centerPoints.Count)
        {
            Debug.LogError("The number of radii must match the number of center points.");
            return;
        }

        // Remove any extra circles
        while (circles.Count > centerPoints.Count)
        {
            Destroy(circles[circles.Count - 1]);
            circles.RemoveAt(circles.Count - 1);
        }

        // Update or create circles
        for (int i = 0; i < centerPoints.Count; i++)
        {
            Vector3 centerPoint = centerPoints[i];
            float radius = radii[i];

            if (i < circles.Count)
            {
                // Update an existing circle
                GameObject circle = circles[i];
                circle.transform.position = centerPoint;

                SetLinePosition(circle,centerPoint,radius);
            }
            else
            {
                // Create a new circle
                GameObject circle = Instantiate(circlePrefab, centerPoint, Quaternion.identity, transform);
                SetLinePosition(circle,centerPoint,radius);

                circles.Add(circle);
            }
        }
    }

    public void SetLinePosition(GameObject circle,Vector3 centerPoint,float radius)
    {
        circle.transform.position = centerPoint;

        LineRenderer lineRenderer = circle.GetComponent<LineRenderer>();
        lineRenderer.positionCount = numSegments + 1;
        lineRenderer.loop = true;
        Vector3[] positions = new Vector3[numSegments + 1];

        for (int j = 0; j <= numSegments; j++)
        {
            float angle = (float)j / (float)numSegments * Mathf.PI * 2.0f;
            float x = Mathf.Cos(angle) * radius + centerPoint.x;
            float y = Mathf.Sin(angle) * radius + centerPoint.y;
            positions[j] = new Vector3(x, y, centerPoint.z);
        }
        lineRenderer.SetPositions(positions);
    }
}
