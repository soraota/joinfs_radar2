using UnityEngine;

public class CameraControl : MonoBehaviour
{
    private bool isDragging = false;
    private Vector3 dragOrigin;
    public Camera MainCamera;
    public float dragSpeed = 2f; // ドラッグで動かすスピード
    public float zoomSpeed = 2f; // マウスホイールでの拡大縮小スピード
    public float maxZoom = 5f; // 最大ズーム値
    public float minZoom = 1f; // 最小ズーム値

    void LateUpdate()
    {
        if (Input.GetMouseButtonDown(0))
        {
            isDragging = true;
            dragOrigin = Input.mousePosition;
        }

        if (Input.GetMouseButtonUp(0))
        {
            isDragging = false;
        }

        if (isDragging)
        {
            Vector3 pos = Camera.main.ScreenToViewportPoint(dragOrigin - Input.mousePosition);
            Vector3 move = new Vector3(pos.x * dragSpeed, pos.y * dragSpeed, 0);

            transform.Translate(move, Space.World);

            dragOrigin = Input.mousePosition;
        }

        float zoom = Input.GetAxis("Mouse ScrollWheel");
        if (zoom != 0)
        {
            float newSize = MainCamera.orthographicSize - zoom * zoomSpeed;
            MainCamera.orthographicSize = Mathf.Clamp(newSize, minZoom, maxZoom);
        }
    }
}
