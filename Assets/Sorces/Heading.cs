using UnityEngine;

public class Heading : MonoBehaviour
{
    public float alt;
    public float angle;
    public float offset_angle;
    public Vector3 Airplane_direction;
    public string colorstring = "#00FF14";
    private Color line_color;
    Vector3 Z1 = new Vector3(0,0,1);

    private LineRenderer lineRenderer;

    void Start()
    {
        offset_angle = -50;

        ColorUtility.TryParseHtmlString(colorstring, out line_color);

        //lineRendererの設定
        lineRenderer = GetComponent<LineRenderer>();
        lineRenderer.positionCount = 2;
        lineRenderer.startWidth = 0.024f;
        lineRenderer.endWidth = 0.024f;
        lineRenderer.material = new Material(Shader.Find("Sprites/Default"));
        lineRenderer.material.color = line_color;
    }

    void Update()
    {
        render_Heading_Bou();

    }
    void render_Heading_Bou()
    {
        if (alt > 1000) //1000ft以上だったら
        {
            //向いている方位を示す棒を描画
            Vector3 direction = transform.right;

            lineRenderer.SetPosition(0, transform.position + Z1);
            lineRenderer.SetPosition(1, transform.position + direction.normalized + Z1);
            /*float radian = (angle + offset_angle) * Mathf.Deg2Rad;
            Vector3 direction = new Vector3(Mathf.Cos(radian), Mathf.Sin(radian), 0);

            lineRenderer.SetPosition(0, transform.position + Z1);
            lineRenderer.SetPosition(1, (transform.position + direction.normalized) + Z1);*/
        }
        //地上だったら(1000ft以下)
        else
        {
            lineRenderer.SetPosition(0, transform.position - Z1);
            lineRenderer.SetPosition(1, transform.position - Z1);
        }
    }
}
